#include "driver_aux.h"

#include <string>
#include <fstream>
#include <stdexcept>
#include <iomanip>

#include <signal.h>

#include "driver.h"

using std::cout;
using std::cerr;

typedef std::map<const std::string, command_fn> CommandDict;

struct ParamSet
{
  typedef std::map<const std::string, Param*> ParamDict;
  ParamDict params;

  void set(const std::string& name, const std::string& val);
  std::string get(const std::string& name);
};

void ParamSet::set(const std::string& name, const std::string& val)
{
  ParamDict::iterator elem = params.find(name);
  if (elem == params.end())
    throw std::runtime_error("Unknown parameter " + name);

  elem->second->parse_and_set(val);
}

std::string ParamSet::get(const std::string& name)
{
  ParamDict::iterator elem = params.find(name);
  if (elem == params.end())
    throw std::runtime_error("Unknown parameter " + name);

  return elem->second->get();
}


ParamSet params;
CommandDict commands;

volatile bool sigint = false;
void handle_sigint(int s)
{
  sigint = true;
}


std::string readline(std::istream& cin)
{
  char c = 0;
  std::stringstream in;
  bool comment = (cin.peek() == '#');

  while(cin.good())
    {
      c = cin.get();
      if (c == '\n' or !cin.good())
        break;

      in << c;
    }

  if (comment)
    return "";

  return in.str();
}

command_t read_command(std::istream& cin)
{
  command_t tokens;
  std::string str = readline(cin);
  tokenize(str, tokens, " ", true, 2);
  return tokens;
}



bool process_command(const command_t& cmd)
{
  if (cmd[0] == ".q")
    return false;
  else if (cmd[0][0] == '.')
    {
      CommandDict::iterator elem = commands.find(cmd[0]);
      if (elem == commands.end())
        throw std::runtime_error("Unknown command " + cmd[0]);

      elem->second(cmd);
      cout << "done\n";
      return true;
    }

  if (cmd.size() == 1)
    {
      std::string v = params.get(cmd[0]);
      cout << cmd[0] << " = " << v << "\n";
      return true;
    }
  if (cmd.size() == 2)
    {
      std::string v = params.get(cmd[0]);
      cout << "old " << cmd[0] << " = " << v << "\n";
      params.set(cmd[0], cmd[1]);
      cout << "new " << cmd[0] << " = " << params.get(cmd[0]) << "\n";
      return true;
    }

  return true;
}

bool file_loop(const char* name)
{
  std::ifstream fin(name,  std::ifstream::in);
  while (fin.good())
    try
      {
        command_t cmd = read_command(fin);
        if (cmd.size() == 0)
          continue;
        if (! process_command(cmd))
          return false;
        if (sigint)
          break;
      }
    catch (std::runtime_error &e)
      {
        cout << "error while executing file: " << e.what() << "\n";
        break;
      }

  return true;
}

void command_loop()
{
  bool repeat = true;
  while (repeat)
    {
      try
        {
          if (!std::cin.good())
            break;

          cout << "> ";
          command_t cmd = read_command(std::cin);
          if (cmd.size() == 0)
            continue;

          repeat = process_command(cmd);
        }
      catch (std::runtime_error &e)
        {
          cout << "error while executing command\n" << e.what() << "\n";
        }
    }
}

void init_params(const std::vector<ParamDesc>& params)
{
  for (auto& p : params)
    ::params.params[p.name] = p.param;
}

void init_commands(const std::vector<CommandDesc>& commands)
{
  for (auto& c : commands)
    ::commands[c.name] = c.command;
}
