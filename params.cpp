#include <stdexcept>
#include <stdlib.h>
#include <limits.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <sstream>
#include <iostream>
#include <iomanip>

#include "params.h"
#include "driver_aux.h"

int str2int(const std::string& str)
{
  char *endptr = 0;
  const char *cstr = str.c_str();
  int val;

  errno = 0;    /* To distinguish success/failure after call */
  val = strtol(cstr, &endptr, 10);

  /* Check for various possible errors */
  if ((errno == ERANGE && (val == LONG_MAX || val == LONG_MIN))
      || (errno != 0 && val == 0))
    throw std::runtime_error(strerror(errno));

  if (endptr == str)
    throw std::runtime_error("No digits were found");

  if (*endptr != '\0')
    throw std::runtime_error("Further characters after number.");

  return val;
}

template <class T>
std::string tostring(T val)
{
  std::stringstream out;
  out << val;
  return out.str();
}

double str2double(const std::string& str)
{
  char *endptr = 0;
  const char *cstr = str.c_str();
  double val;

  errno = 0;    /* To distinguish success/failure after call */
  val = strtod(cstr, &endptr);

  /* Check for various possible errors */
  if ((errno == ERANGE && (val == LONG_MAX || val == LONG_MIN))
      || (errno != 0 && val == 0))
    throw std::runtime_error(strerror(errno));

  if (endptr == str)
    throw std::runtime_error("No digits were found");

  if (*endptr != '\0')
    throw std::runtime_error("Further characters after number");

  return val;
}


void IntParam::parse_and_set(const std::string& val)
{
  *pval = str2int(val);
}

std::string IntParam::get()
{
  return tostring(*pval);
}

void BoolParam::parse_and_set(const std::string& val)
{
  int v = str2int(val);
  if (v == 0 || v == 1)
    *pval = v;
  else
    throw std::runtime_error("Incorrect logical value. Supply 0 or 1.");
}

std::string BoolParam::get()
{
  return tostring(*pval);
}


void DoubleParam::parse_and_set(const std::string& val)
{
  *pval = str2double(val);
}

std::string DoubleParam::get()
{
  return tostring(*pval);
}

void StringParam::parse_and_set(const std::string& val)
{
  *pval = val;
}

std::string StringParam::get()
{
  return *pval;
}

void StreamParam::parse_and_set(const std::string& val)
{
  command_t tokens;
  tokenize(val, tokens, " ", true, 2);

  std::string mode = tokens[0];
  std::string path = tokens[1];

  std::ios_base::openmode openmode = std::ios_base::out;

  if (mode == "trunc")
    openmode |= std::ios_base::trunc;
  else if (mode == "append")
    openmode |= std::ios_base::app;
  else
    throw std::runtime_error("invalid mode");

  std::ostream* str = nullptr;

  if (path == "$std")
    str = &std::cout;
  else
    {
      str = new std::ofstream(path, openmode);
      if (str->fail())
        {
          delete str;
          throw std::runtime_error("cannot open file");
        }
    }

  (*str) << std::setprecision(15);

  if (pval && (*pval))
    {
      (*pval)->flush();
      if ((*pval) != &std::cout)
        delete *pval;
    }

  *pval = str;
  filename = path;
}

std::string StreamParam::get()
{
  return filename;
}
