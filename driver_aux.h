#ifndef DRIVER_AUX_H
#define DRIVER_AUX_H

#include <string>
#include <vector>
#include <sstream>
#include <iostream>
#include <fstream>

template < class ContainerT >
void tokenize(const std::string& str, ContainerT& tokens,
              const std::string& delimiters = " ",
              const bool trimEmpty = false,
              const int max_tokens = 0)
{
  std::string::size_type pos, lastPos = 0;
  int tk_num = 0;
  while(true)
    {
      if (max_tokens == 0 || tk_num < max_tokens-1)
        pos = str.find_first_of(delimiters, lastPos);
      else
        pos = std::string::npos;

      if (pos == std::string::npos)
        {
          pos = str.length();

          if (pos != lastPos || !trimEmpty)
            {
            tokens.push_back(typename ContainerT::value_type(str.data()+lastPos,
                                                    (typename ContainerT::value_type::size_type)(pos-lastPos) ));
            ++tk_num;
            }

          break;
        }
      else
        {
          if (pos != lastPos || !trimEmpty)
            {
            tokens.push_back(typename ContainerT::value_type(str.data()+lastPos,
                                                    (typename ContainerT::value_type::size_type)(pos-lastPos)));
            ++tk_num;
            }
        }

      lastPos = pos + 1;
    }
};

typedef std::vector<std::string> command_t;


#endif
