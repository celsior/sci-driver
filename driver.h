#ifndef PARAMS_H
#define PARAMS_H

#include <string>
#include <map>
#include <iostream>

#include "driver_aux.h"

int str2int(const std::string& str);
double str2double(const std::string& str);

struct Param
{
  virtual void parse_and_set(const std::string& val) = 0;
  virtual std::string get() = 0;
};

struct IntParam : public Param
{
  int* pval;
  IntParam(int* pval)
  : pval(pval) {};

  void parse_and_set(const std::string& val);
  std::string get();
};

struct BoolParam : public Param
{
  bool* pval;
  BoolParam(bool* pval)
  : pval(pval) {};

  void parse_and_set(const std::string& val);
  std::string get();
};


struct DoubleParam : public Param
{
  double* pval;
  DoubleParam(double* pval)
  : pval(pval) {};

  void parse_and_set(const std::string& val);
  std::string get();
};

struct StringParam : public Param
{
  std::string* pval;
  StringParam(std::string* pval)
  : pval(pval) {};

  void parse_and_set(const std::string& val);
  std::string get();
};

struct StreamParam : public Param
{
  std::ostream** pval;
  std::string filename;
  StreamParam(std::ostream** pval)
  : pval(pval) {};

  void parse_and_set(const std::string& val);
  std::string get();
};



typedef void (*command_fn)(const command_t& args);


extern volatile bool sigint;

struct ParamDesc
{
  std::string name;
  Param* param;
};

struct CommandDesc
{
  std::string name;
  command_fn command;
};

void init_params(const std::vector<ParamDesc>& params);
void init_commands(const std::vector<CommandDesc>& commands);
bool file_loop(const char* name);
void command_loop();
void handle_sigint(int s);


#endif
